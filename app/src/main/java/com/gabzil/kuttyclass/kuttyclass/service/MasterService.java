package com.gabzil.kuttyclass.kuttyclass.service;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.gabzil.kuttyclass.kuttyclass.R;
import com.gabzil.kuttyclass.kuttyclass.interfacee.OnTaskEnd;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;

public class MasterService extends AsyncTask<Object, String, String> {
    private OnTaskEnd mCallback;
    public ProgressDialog progressDialog;
    private Context context;
    private int flag;

    public MasterService(Context context, OnTaskEnd listner, int flag) {
        this.mCallback = listner;
        this.context=context;
        this.flag=flag;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        try{
       progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
       progressDialog.setIndeterminate(true);
       progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
       progressDialog.show();
        } catch (Exception e) {
            Toast.makeText(context, "Some problem occured, please try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();
        if(flag==1) {
            mCallback.onTaskEndFirst(s);
        }else if(flag==2){
            mCallback.onTaskEndSecond(s);
        }else if(flag==3){
            mCallback.onTaskEndThird(s);
        }
    }

    @Override
    protected String doInBackground(Object... params) {
        int urlStr = (Integer)  params[0];
        String stringData = (String)  params[1];
        String url="http://www.billionwins.com:8086/CustomerDiagnostic/custdiag/WebService/";
        //String url="http://gabretailprod.cloudapp.net:8081/DiagnosticDetails/dcw/DCWService/";
        switch (urlStr){
            case 1: url=url+"GetHome";
                break;
            case 2: url=url+"GetPackages";
                break;
            case 3: url=url+"InsertUser";
                break;
            case 4: url=url+"InsertOrder";
                break;
            case 5: url=url+"InsertAddress";
                break;
            case 6: url=url+"GetUser";
                break;
            case 7: url=url+"GetOrder";
                break;
            case 8: url=url+"GetDiagnostics";
                break;
            case 9: url=url+"GetAddress";
                break;
//            case 10: url=url+"RemoveAddress";
//                break;
        }

        return ServerCon(url,stringData);
    }

    private String ServerCon(String url,String data) {
        String reply = null;
        URI uri = null;
        try {
            uri = new URI(url);
            HttpURLConnection conn = (HttpURLConnection) uri.toURL().openConnection();
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("User-Agent", "Pigeon");
            conn.setChunkedStreamingMode(0);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.connect();
            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            out.write(data.toString().getBytes());
            out.flush();
            int code = conn.getResponseCode();
            String message = conn.getResponseMessage();
            InputStream in1 = conn.getInputStream();
            StringBuffer sb = new StringBuffer();
            try {
                int chr;
                while ((chr = in1.read()) != -1) {
                    sb.append((char) chr);
                }
                reply = sb.toString();
            } finally {
                in1.close();
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return reply;
    }
}
