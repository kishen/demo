package com.gabzil.kuttyclass.kuttyclass.entity;

public class TestModel {
    private int CatID;
    private String CatName;
    private int TestID;
    private int DiseasesID;
    private String DiseasesName;
    private String TestName;
    private String Synonyms;
    private String TestSpecification;
    private String TestDescription;
    private String RiskFactor;
    private String Symptoms;
    private String Gender;
    private String Tag2;
    private String OldPrice;
    private String NewPrice;
    private String TestTiming;
    private String ReportingTime;
    private String AutoDiagHigh;
    private String AutoDiagLow;
    private String AutoDiagTestPositive;
    private String AutoDiagTestNegative;
    private boolean IsActive;
    private String EntryDate;

    public int getCatID() {
        return CatID;
    }

    public void setCatID(int catID) {
        CatID = catID;
    }

    public String getCatName() {
        return CatName;
    }

    public void setCatName(String catName) {
        CatName = catName;
    }

    public int getTestID() {
        return TestID;
    }

    public void setTestID(int testID) {
        TestID = testID;
    }

    public int getDiseasesID() {
        return DiseasesID;
    }

    public void setDiseasesID(int diseasesID) {
        DiseasesID = diseasesID;
    }

    public String getDiseasesName() {
        return DiseasesName;
    }

    public void setDiseasesName(String diseasesName) {
        DiseasesName = diseasesName;
    }

    public String getTestName() {
        return TestName;
    }

    public void setTestName(String testName) {
        TestName = testName;
    }

    public String getSynonyms() {
        return Synonyms;
    }

    public void setSynonyms(String synonyms) {
        Synonyms = synonyms;
    }

    public String getTestSpecification() {
        return TestSpecification;
    }

    public void setTestSpecification(String testSpecification) {
        TestSpecification = testSpecification;
    }

    public String getTestDescription() {
        return TestDescription;
    }

    public void setTestDescription(String testDescription) {
        TestDescription = testDescription;
    }

    public String getRiskFactor() {
        return RiskFactor;
    }

    public void setRiskFactor(String riskFactor) {
        RiskFactor = riskFactor;
    }

    public String getSymptoms() {
        return Symptoms;
    }

    public void setSymptoms(String symptoms) {
        Symptoms = symptoms;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getTag2() {
        return Tag2;
    }

    public void setTag2(String tag2) {
        Tag2 = tag2;
    }

    public String getOldPrice() {
        return OldPrice;
    }

    public void setOldPrice(String oldPrice) {
        OldPrice = oldPrice;
    }

    public String getNewPrice() {
        return NewPrice;
    }

    public void setNewPrice(String newPrice) {
        NewPrice = newPrice;
    }

    public String getTestTiming() {
        return TestTiming;
    }

    public void setTestTiming(String testTiming) {
        TestTiming = testTiming;
    }

    public String getReportingTime() {
        return ReportingTime;
    }

    public void setReportingTime(String reportingTime) {
        ReportingTime = reportingTime;
    }

    public String getAutoDiagHigh() {
        return AutoDiagHigh;
    }

    public void setAutoDiagHigh(String autoDiagHigh) {
        AutoDiagHigh = autoDiagHigh;
    }

    public String getAutoDiagLow() {
        return AutoDiagLow;
    }

    public void setAutoDiagLow(String autoDiagLow) {
        AutoDiagLow = autoDiagLow;
    }

    public String getAutoDiagTestPositive() {
        return AutoDiagTestPositive;
    }

    public void setAutoDiagTestPositive(String autoDiagTestPositive) {
        AutoDiagTestPositive = autoDiagTestPositive;
    }

    public String getAutoDiagTestNegative() {
        return AutoDiagTestNegative;
    }

    public void setAutoDiagTestNegative(String autoDiagTestNegative) {
        AutoDiagTestNegative = autoDiagTestNegative;
    }

    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean isActive) {
        IsActive = isActive;
    }

    public String getEntryDate() {
        return EntryDate;
    }

    public void setEntryDate(String entryDate) {
        EntryDate = entryDate;
    }
}
