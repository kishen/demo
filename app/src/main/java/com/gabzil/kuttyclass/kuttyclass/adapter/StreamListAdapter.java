package com.gabzil.kuttyclass.kuttyclass.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gabzil.kuttyclass.kuttyclass.R;
import com.gabzil.kuttyclass.kuttyclass.entity.DiagnosticPackageModel;

import java.util.ArrayList;

/**
 * Created by Kalyan on 28-04-2017.
 */


public class StreamListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<DiagnosticPackageModel> testList;
    public StreamListAdapter( ArrayList<DiagnosticPackageModel> testList ){
    this.testList=testList;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.streamlistadapter, parent, false);
        return new StreamListHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        StreamListHolder hoolder=(StreamListHolder)holder;
        hoolder.stream1.setText(testList.get(position).getPackageName());
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }
    public class StreamListHolder extends RecyclerView.ViewHolder {
        TextView stream1;
        public StreamListHolder(View itemView) {
            super(itemView);
          stream1= (TextView) itemView.findViewById(R.id.stream1);
        }
    }
}
