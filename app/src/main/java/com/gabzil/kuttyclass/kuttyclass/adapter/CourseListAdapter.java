package com.gabzil.kuttyclass.kuttyclass.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gabzil.kuttyclass.kuttyclass.R;
import com.gabzil.kuttyclass.kuttyclass.entity.DiagnosticPackageModel;

import java.util.ArrayList;

/**
 * Created by Kalyan on 26-04-2017.
 */

public class CourseListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<DiagnosticPackageModel> testList;
    public CourseListAdapter( ArrayList<DiagnosticPackageModel> testList ) {
        this.testList = testList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.courselistadapter, parent, false);
        return new CourseListHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CourseListHolder hoolder=(CourseListHolder)holder;
        hoolder.text.setText(testList.get(position).getPackageDescription());
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }

    public class CourseListHolder extends RecyclerView.ViewHolder {
        TextView text;
        public CourseListHolder(View itemView) {
            super(itemView);
            text= (TextView) itemView.findViewById(R.id.coursetext);


        }
    }

}

