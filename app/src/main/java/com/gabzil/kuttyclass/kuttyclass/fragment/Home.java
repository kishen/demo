package com.gabzil.kuttyclass.kuttyclass.fragment;


import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gabzil.kuttyclass.kuttyclass.R;
import com.gabzil.kuttyclass.kuttyclass.adapter.CourseListAdapter;
import com.gabzil.kuttyclass.kuttyclass.adapter.StreamListAdapter;
import com.gabzil.kuttyclass.kuttyclass.entity.DiagnosticPackageModel;
import com.gabzil.kuttyclass.kuttyclass.interfacee.OnTaskEnd;
import com.gabzil.kuttyclass.kuttyclass.service.MasterService;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONStringer;

import java.util.ArrayList;


public class Home extends android.app.Fragment implements OnTaskEnd {
    StreamListAdapter adapter;

    RecyclerView recyclerView;
    ArrayList<DiagnosticPackageModel> testList = new ArrayList<DiagnosticPackageModel>();

    public Home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView= (RecyclerView) view.findViewById(R.id.recycleview);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        call();
        return view;

    }
    public void call() {
        try {
//            if (isNetworkAvaliable(getActivity())) {
            JSONStringer shop = new JSONStringer()
                    .object()
                    .key("CatInfo")
                    .object()
                    .key("CatID").value(1)
                    .endObject()
                    .endObject();
            serviceCaller(shop.toString(),2,1);
//            } else {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                ft.add(R.id.content_main, new ConnectionLost()).commit();
//
//                Toast.makeText(getActivity(), "Please check the connection", Toast.LENGTH_LONG).show();
//            }
        } catch (Exception e) {
//            Toast.makeText(this, "Some problem occured, Packages not found", Toast.LENGTH_SHORT).show();
        }
    }
    @TargetApi(Build.VERSION_CODES.M)
    private void serviceCaller(String info, int serno, int caller) {
        try {
//            if (MainActivity.isNetworkAvaliable(getActivity())) {
            final MasterService task = new MasterService(getContext(), this, caller);
            task.execute(serno, info);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (task.getStatus() == AsyncTask.Status.RUNNING) {
                        task.cancel(true);
                        task.progressDialog.dismiss();
//                            showMessage("Network Problem, Please try again");
                    }
                }
            }, 1000 * 30);
//            } else {
//                showMessage("Check your internet connection");

        } catch (Exception e) {
            e.printStackTrace();
//            showMessage("Some problem occured");
        }
    }


    @Override
    public void onTaskEndFirst(String results) {
        try {
            if(results!=null) {
                if(results.equals("null")){
//                    Toast.makeText(this, "Server error, Test not found", Toast.LENGTH_SHORT).show();
                }else {
                    Gson gson = new Gson();
                    JSONArray arr = new JSONArray(results);
                    if(arr.length()>0) {
                        for (int i = 0; i < arr.length(); i++) {
                            DiagnosticPackageModel test = gson.fromJson(arr.get(i).toString(), DiagnosticPackageModel.class);
                            //packagesList = resul.getPackageArray();
                            testList.add(test);
                        }
                    }else{
//                        Toast.makeText(this, "Test not found", Toast.LENGTH_SHORT).show();
                    }
                }
                boolean rFlag=true;
                CourseListAdapter testAdapter = new CourseListAdapter(testList);
                recyclerView.setAdapter(testAdapter);
//            }else{
//                Toast.makeText(this, "Some problem occured, Test not found", Toast.LENGTH_SHORT).show();
//            }

            }}  catch (Exception e) {
//            Toast.makeText(this, "Some problem occured, Test not found", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onTaskEndSecond(String result) {

    }

    @Override
    public void onTaskEndThird(String result) {

    }
}
