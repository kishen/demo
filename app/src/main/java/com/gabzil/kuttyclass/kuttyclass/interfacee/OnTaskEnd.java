package com.gabzil.kuttyclass.kuttyclass.interfacee;

public interface OnTaskEnd {
    public void onTaskEndFirst(String result);
    public void onTaskEndSecond(String result);
    public void onTaskEndThird(String result);
}
