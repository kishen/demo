package com.gabzil.kuttyclass.kuttyclass.entity;

import java.util.List;

public class DiagnosticPackageModel {
    private int DiagID;
    private int PackageID;
    private int DiseasesID;
    private int AgeID;
    private String PackageName;
    private int CatID;
    private String CatName;
    private String DiagName;
    private String PackageDescription;
    private String Gender;
    private String ImageUrl;
    private String AgeGroup;
    private String FamilyHistory;
    private String RiskFactor;
    private String DiseasesName;
    private String Symptoms;
    private String StandardPrice;
    private String OfferedPrice;
    private double Rating;
    private boolean IsSingle;
    private boolean IsActive;
    private String EntryDate;
    private String Fasting;
    private List<TestModel> AllTest;

    public int getDiagID() {
        return DiagID;
    }

    public void setDiagID(int diagID) {
        DiagID = diagID;
    }

    public int getPackageID() {
        return PackageID;
    }

    public void setPackageID(int packageID) {
        PackageID = packageID;
    }

    public int getDiseasesID() {
        return DiseasesID;
    }

    public void setDiseasesID(int diseasesID) {
        DiseasesID = diseasesID;
    }

    public int getAgeID() {
        return AgeID;
    }

    public void setAgeID(int ageID) {
        AgeID = ageID;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }

    public int getCatID() {
        return CatID;
    }

    public void setCatID(int catID) {
        CatID = catID;
    }

    public String getCatName() {
        return CatName;
    }

    public void setCatName(String catName) {
        CatName = catName;
    }

    public String getDiagName() {
        return DiagName;
    }

    public void setDiagName(String diagName) {
        DiagName = diagName;
    }

    public String getPackageDescription() {
        return PackageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        PackageDescription = packageDescription;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getAgeGroup() {
        return AgeGroup;
    }

    public void setAgeGroup(String ageGroup) {
        AgeGroup = ageGroup;
    }

    public String getFamilyHistory() {
        return FamilyHistory;
    }

    public void setFamilyHistory(String familyHistory) {
        FamilyHistory = familyHistory;
    }

    public String getRiskFactor() {
        return RiskFactor;
    }

    public void setRiskFactor(String riskFactor) {
        RiskFactor = riskFactor;
    }

    public String getDiseasesName() {
        return DiseasesName;
    }

    public void setDiseasesName(String diseasesName) {
        DiseasesName = diseasesName;
    }

    public String getSymptoms() {
        return Symptoms;
    }

    public void setSymptoms(String symptoms) {
        Symptoms = symptoms;
    }

    public String getStandardPrice() {
        return StandardPrice;
    }

    public void setStandardPrice(String standardPrice) {
        StandardPrice = standardPrice;
    }

    public String getOfferedPrice() {
        return OfferedPrice;
    }

    public void setOfferedPrice(String offeredPrice) {
        OfferedPrice = offeredPrice;
    }

    public double getRating() {
        return Rating;
    }

    public void setRating(double rating) {
        Rating = rating;
    }

    public boolean getIsSingle() {
        return IsSingle;
    }

    public void setIsSingle(boolean isSingle) {
        IsSingle = isSingle;
    }

    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean isActive) {
        IsActive = isActive;
    }

    public String getEntryDate() {
        return EntryDate;
    }

    public void setEntryDate(String entryDate) {
        EntryDate = entryDate;
    }

    public List<TestModel> getAllTest() {
        return AllTest;
    }

    public void setAllTest(List<TestModel> allTest) {
        AllTest = allTest;
    }

    public String getFasting() {
        return Fasting;
    }

    public void setFasting(String fasting) {
        Fasting = fasting;
    }
}
